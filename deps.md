Added to vendor 

- [x] github.com/kyoh86/xdg
- [x] github.com/emersion/go-imap-idle
- [x] github.com/emersion/go-maildir
- [x] git.sr.ht/~sircmpwn/getopt
- [x] github.com/emersion/go-imap
- [x] github.com/miolini/datacounter 
- [x] github.com/emersion/go-message
- [x] github.com/riywo/loginshell
- [x] github.com/emersion/go-smtp 
- [x] git.sr.ht/~sircmpwn/pty 
- [x] github.com/emersion/go-sasl
- [x] github.com/ddevault/go-libvterm
