Source: aerc
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Karthik <kskarthik@disroot.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-danwakefield-fnmatch-dev,
               golang-github-gdamore-tcell-dev,
               golang-github-go-fsnotify-fsnotify-dev,
               golang-github-go-ini-ini-dev,
               golang-github-google-shlex-dev,
               golang-github-mattn-go-isatty-dev,
               golang-github-mattn-go-runewidth-dev,
               golang-github-mitchellh-go-homedir-dev,
               golang-github-pkg-errors-dev,
               golang-github-stretchr-testify-dev,
               golang-golang-x-oauth2-google-dev,
               scdoc
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/aerc
Vcs-Git: https://salsa.debian.org/go-team/packages/aerc.git
Homepage: https://aerc-mail.org
Rules-Requires-Root: no
XS-Go-Import-Path: git.sr.ht/~sircmpwn/aerc

Package: aerc
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: The World's Best Email Client
 Some of its more interesting features include:
    - Editing emails in an embedded terminal tmux-style, allowing you to check on incoming emails and reference other threads while you compose your replies
    - Render HTML emails with an interactive terminal web browser, highlight patches with diffs, and browse with an embedded less session
    - Vim-style keybindings and ex-command system, allowing for powerful automation at a single keystroke
    - First-class support for working with git & email
    - Open a new tab with a terminal emulator and a shell running for easy access to nearby git repos for parallel work
    - Support for multiple accounts, with support for IMAP, Maildir, SMTP, and sendmail transfer protocols
    - Asynchronous IMAP support ensures the UI never gets locked up by a flaky network, as mutt often does
    - Efficient network usage - aerc only downloads the information which is necessary to present the UI, making for a snappy and bandwidth-efficient experience
    100% free and open source software!
